#!/usr/bin/env python3

#
# Script to adjust the PILS device table, typically in GVL_PILS.TcGVL
# Adjust all the offsets: aling data to 16/32/64 bit boundaries.
# Make sure that devices don't overlap
# This is
# https://forge.frm2.tum.de/public/doc/plc/master/singlehtml/

import glob
import re
import sys

RE_MATCH_DEVICE_AT_MB = re.compile(
    #    stPTPOffset     AT %MB100: ST_1202;
    # Devices start with 0..7: ST_0xyz .. ST_7xyz. Hex values are allowed
    r"(\s*)(\S*)(\s*)(AT)(\s*)(%MB)(\d+)(\D+)(\s+)(ST_)([0-7][a-fA-F0-9]*)(.*)$"
)

RE_MATCH_DEVICE_PILS_OFFSET = re.compile(
#        (nTypCode := 16#1204, sName := 'SystemUTCtime',       nOffset := 112, nUnit := 16#F711),
#        (nTypCode := 16#1202, sName := 'PTPOffset',           nOffset := 100),
    r"(\s*.nTypCode\s*:=\s*16#)([0-9a-fA-F]+)(,[^,]+,\s*)(nOffset\s*:=\s*)(\d+)(\D+.*)$"
)

RE_MATCH_DEVICE_PILS_MOTOR_AT_MB = re.compile(
#        fbMotorM1: FB_5010_Axis := (nPILSDeviceNumber := 9);
    r"(.*)(nPILSDeviceNumber\s*:=\s*)(\d+)(\D+.*)$"
)


pils_device_byte_aligments = {
    '1201' :  '2', # Simple discrete input, 16 bit signed integer
    '1202' :  '4', # Simple discrete input, 32 bit signed integer
    '1204' :  '8', # Simple discrete input, 64 bit signed integer
    '1302' :  '4', # Simple analog input, 32 bit floating point, real
    '1304' :  '8', # Simple analog input, 64 bit floating point, double
    '1602' :  '2', # Simple discrete output, 16 bit signed integer
    '1604' :  '4', # Simple discrete output, 32 bit signed integer
    '1608' :  '8', # Simple discrete output, 64 bit signed integer
    '1704' :  '4', # Simple analog output, 32 bit floating point, real
    '1708' :  '8', # Simple analog output, 64 bit floating point, double
    '1802' :  '4', # Extended status word that has 24 AUX bits
    '1A04' :  '4', # discrete input, 32 bit signed integer + extended status word
    '1A08' :  '8', # discrete input, 64 bit signed integer + extended status word + errorID
    '1B04' :  '4', # analog input, 32 bit floating point + status word
    '1B08' :  '8', # analog input, 64 bit floating point + extended status word + errorID
    '1E04' :  '4', # discrete output, 16 bit signed integer + extended status word
    '1E06' :  '4', # discrete output, 32 bit signed integer + extended status word
    '1E0C' :  '8', # discrete output, 64 bit signed integer + extended status word + errorID
    '1F06' :  '4', # analog output, 32 bit signed integer + extended status word
    '1F0C' :  '8', # analog output, 64 bit signed integer + extended status word + errorID
    '5010' :  '8'  # param device with 64 bit float, motor
}

pils_device_byte_lengths = {
    '1201' :  '2',
    '1202' :  '4',
    '1204' :  '8',
    '1302' :  '4',
    '1304' :  '8',
    '1602' :  '4',
    '1604' :  '8',
    '1608' :  '16',
    '1704' :  '4',
    '1708' :  '8',
    '1802' :  '4',
    '1A04' :  '8',
    '1A08' :  '16',
    '1B04' :  '8',
    '1B08' :  '16',
    '1E04' :  '8',
    '1E06' :  '12',
    '1E0C' :  '24',
    '1F06' :  '12',
    '1F0C' :  '24',
    '5010' :  '32'
}

found_pils_device_types = []

def help_and_exit():
    print(f"{sys.argv[0]} : [--fix|--debug] <filename> [<filename>]")
    print("./check_fix_gvl_pils.py  --fix ../tc_pils_app_specific/GVLs/GVL_PILS.TcGVL")
    print("./check_fix_gvl_pils.py ../tc_pils_app_specific/GVLs/GVL_PILS.TcGVL")
    exit(2)


def parse_line_device_at_mb(line, debug):
    global device_at_mb_index
    global device_at_mb_start_offsets
    global device_at_mb_last_offset

    match_device_at_mb = RE_MATCH_DEVICE_AT_MB.match(line)
    if match_device_at_mb != None:
        device_at_mb_index = device_at_mb_index + 1
        if debug >= 3:
            print(f"match match_device_at_mb line={line}")
        gidx = 1
        match1 = match_device_at_mb.group(gidx)
        gidx = gidx + 1
        match2 = match_device_at_mb.group(gidx)
        gidx = gidx + 1
        match3 = match_device_at_mb.group(gidx)
        gidx = gidx + 1
        match4 = match_device_at_mb.group(gidx)
        gidx = gidx + 1
        match5 = match_device_at_mb.group(gidx)
        gidx = gidx + 1
        match6 = match_device_at_mb.group(gidx)
        gidx = gidx + 1
        match7 = match_device_at_mb.group(gidx)
        gidx = gidx + 1
        match8 = match_device_at_mb.group(gidx)
        gidx = gidx + 1
        match9 = match_device_at_mb.group(gidx)
        gidx = gidx + 1
        match10 = match_device_at_mb.group(gidx)
        gidx = gidx + 1
        match11 = match_device_at_mb.group(gidx)
        gidx = gidx + 1
        match12 = match_device_at_mb.group(gidx)
        gidx = gidx + 1
        if debug >= 3:
            print(f"device_at_mb match1={match1} match2={match2} match3={match3} match4={match4} match5={match5} match6={match6} match7={match7} match8={match8} match9={match9} match10={match10} match11={match11} match12={match12} ")

        pils_device_start_offset = match7
        pils_device_typ = match11.upper()
        found_pils_device_types.append(pils_device_typ)
        pils_device_byte_aligment = pils_device_byte_aligments[pils_device_typ]
        pils_device_byte_length = pils_device_byte_lengths[pils_device_typ]
        if pils_device_byte_aligment == None or pils_device_byte_length == None:
            printf (f"illegal pils_device_typ: {pils_device_typ} line={line}")
            sys.exit (1)
        pils_device_start_offset = int(pils_device_start_offset)
        pils_device_byte_aligment = int(pils_device_byte_aligment)
        pils_device_byte_length = int(pils_device_byte_length)
        if debug >= 3:
            print(f"pils_device_start_offset={pils_device_start_offset} pils_device_byte_aligment={pils_device_byte_aligment} pils_device_byte_length={pils_device_byte_length}")
        # Adjust offset: start after the last device
        pils_device_start_offset = device_at_mb_last_offset
        #
        misaligned = pils_device_start_offset % pils_device_byte_aligment
        if misaligned != 0:
            pils_device_start_offset = pils_device_start_offset - misaligned + pils_device_byte_aligment

        device_at_mb_start_offsets[device_at_mb_index] = pils_device_start_offset

        device_at_mb_last_offset = pils_device_start_offset + pils_device_byte_length
        line = f"{match1}{match2}{match3}{match4}{match5}{match6}{pils_device_start_offset}{match8}{match9}{match10}{match11}{match12}"
        return (line, True)
    else:
        return (line, False)

def parse_line_device_pils_motor_at_mb(line, debug):
    global motor_at_mb_number
    global pils_device_table_index

    match_device_pils_motor_at_mb = RE_MATCH_DEVICE_PILS_MOTOR_AT_MB.match(line)
    if match_device_pils_motor_at_mb != None:
        if debug >= 2:
            print(f"match_device_pils_motor_at_mb line={line}")
        gidx = 1
        match1 = match_device_pils_motor_at_mb.group(gidx)
        gidx = gidx + 1
        match2 = match_device_pils_motor_at_mb.group(gidx)
        gidx = gidx + 1
        match3 = match_device_pils_motor_at_mb.group(gidx)
        gidx = gidx + 1
        match4 = match_device_pils_motor_at_mb.group(gidx)
        gidx = gidx + 1
        motor_pils_device_no = match3
        if debug >= 3:
            print(f"match1={match1} match2={match2} match3={match3} match4={match4}")
            print(f"motor_pils_device_no={motor_pils_device_no} device_at_mb_index={device_at_mb_index} motor_at_mb_number={motor_at_mb_number}")
        match3 = fb_xxx_axis_pilsdevice_number[motor_at_mb_number]
        motor_at_mb_number = motor_at_mb_number + 1
        line = f"{match1}{match2}{match3}{match4}"
        return (line, True)
    else:
        return (line, False)

def parse_line_device_pils_offset(line, debug):
    global device_at_mb_start_offsets
    global pils_device_table_index

    match_device_pils_offset = RE_MATCH_DEVICE_PILS_OFFSET.match(line)
    if match_device_pils_offset != None:
        pils_device_table_index = pils_device_table_index + 1
        if debug >= 3:
            print(f"match_device_pils_offset line={line}")
        gidx = 1
        match1 = match_device_pils_offset.group(gidx)
        gidx = gidx + 1
        match2 = match_device_pils_offset.group(gidx)
        gidx = gidx + 1
        match3 = match_device_pils_offset.group(gidx)
        gidx = gidx + 1
        match4 = match_device_pils_offset.group(gidx)
        gidx = gidx + 1
        match5 = match_device_pils_offset.group(gidx)
        gidx = gidx + 1
        match6 = match_device_pils_offset.group(gidx)
        gidx = gidx + 1
        match7 = '' #match_device_pils_offset.group(gidx)
        gidx = gidx + 1
        match8 = '' # match_device_pils_offset.group(gidx)
        gidx = gidx + 1
        match9 = '' # match_device_pils_offset.group(gidx)
        gidx = gidx + 1
        match10 = '' # match_device_pils_offset.group(gidx)
        gidx = gidx + 1
        match11 = '' # match_device_pils_offset.group(gidx)
        gidx = gidx + 1
        match12 = '' # match_device_pils_offset.group(gidx)
        gidx = gidx + 1
        if debug >= 3:
            print(f"match1='{match1}' match2='{match2}' match3='{match3}' match4='{match4}' match5='{match5}' match6='{match6}' match7='{match7}' match8='{match8}' match9='{match9}' match10='{match10}' match11='{match11}' match12='{match12}'")

        #pils_device_start_offset = match5
        pils_device_typ = match2.upper()
        pils_device_typ_in_mb_table = found_pils_device_types[pils_device_table_index - 1]
        if debug >= 3:
            print(f"pils_device_typ='{pils_device_typ}' pils_device_typ_in_mb_table='{pils_device_typ_in_mb_table}'")
        pils_device_byte_aligment = pils_device_byte_aligments[pils_device_typ]
        pils_device_byte_length = pils_device_byte_lengths[pils_device_typ]
        if pils_device_byte_aligment == None or pils_device_byte_length == None:
            printf (f"illegal pils_device_typ: {pils_device_typ} line={line}")
            match5 = "XerrorIllegalPilsDevice"

        elif pils_device_table_index <= device_at_mb_index:
            match5 = device_at_mb_start_offsets[pils_device_table_index]
        else:
            match5 = "XerrorNotEnoughDevices"

        if pils_device_typ != pils_device_typ_in_mb_table:
            match2 = pils_device_typ_in_mb_table
            print(f"ZZZZ pils_device_typ='{pils_device_typ}' pils_device_typ_in_mb_table='{pils_device_typ_in_mb_table}'")
        line = f"{match1}{match2}{match3}{match4}{match5}{match6}{match7}{match8}{match9}{match10}{match11}{match12}"
        return (line, True)
    else:
        return (line, False)

def parse_line_device_pils_find_motors(lines, debug):
    fb_xxx_axis_pilsdevice_number = []
    pils_device_number = 1 # We start with 1: The indexer is 0
    for line in lines:
        line = line.rstrip("\n")
        line = line.rstrip("\r")
        match_device_pils_offset = RE_MATCH_DEVICE_PILS_OFFSET.match(line)
        if match_device_pils_offset != None:
            if debug >= 3:
                print(f"XXX parse_line_device_pils_find_motors line={line}")
            gidx = 1
            match1 = match_device_pils_offset.group(gidx)
            gidx = gidx + 1
            match2 = match_device_pils_offset.group(gidx)
            gidx = gidx + 1
            match3 = match_device_pils_offset.group(gidx)
            gidx = gidx + 1
            match4 = match_device_pils_offset.group(gidx)
            gidx = gidx + 1
            match5 = match_device_pils_offset.group(gidx)
            gidx = gidx + 1
            match6 = match_device_pils_offset.group(gidx)
            gidx = gidx + 1
            match7 = '' #match_device_pils_offset.group(gidx)
            gidx = gidx + 1
            match8 = '' # match_device_pils_offset.group(gidx)
            gidx = gidx + 1
            match9 = '' # match_device_pils_offset.group(gidx)
            gidx = gidx + 1
            match10 = '' # match_device_pils_offset.group(gidx)
            gidx = gidx + 1
            match11 = '' # match_device_pils_offset.group(gidx)
            gidx = gidx + 1
            match12 = '' # match_device_pils_offset.group(gidx)
            gidx = gidx + 1
            if debug >= 3:
                print(f"XXX match1={match1} match2={match2} match3={match3} match4={match4} match5={match5} match6={match6} match7={match7} match8={match8} match9={match9} match10={match10} match11={match11} match12={match12} ")

            pils_device_typ = match2.upper()
            if pils_device_typ == "5010" or pils_device_typ == "1E04":
                fb_xxx_axis_pilsdevice_number.append(pils_device_number)
                if debug >= 3:
                    print(f"XXXXX motor found pils_device_number={pils_device_number}")
            line = f"{match1}{match2}{match3}{match4}{match5}{match6}{match7}{match8}{match9}{match10}{match11}{match12}"
            pils_device_number = pils_device_number + 1
    return fb_xxx_axis_pilsdevice_number


#
# Handle one file
#
def fix_PILS_GVL_one_file(pathname, debug, fix_files):
    #Where do we start ?
    global device_at_mb_last_offset
    global device_at_mb_index
    global device_at_mb_start_offsets
    global pils_device_table_index
    global motor_at_mb_number
    global fb_xxx_axis_pilsdevice_number

    # indexer at 64, taking 2 + 62 bytes -> 128
    device_at_mb_last_offset = 128
    device_at_mb_index = 0
    pils_device_table_index = 0
    device_at_mb_start_offsets = {}
    last_pils_device_typ = 0
    motor_at_mb_number = 0
    exit_code = 0
    err_line = None
    dirty = False
    new_lines_all = []


    file = open(pathname, 'r', newline='', encoding="iso-8859-1")
    lines = file.readlines()
    file.close()
    line_no = 0
    # Phase one: find motors in this array:
    # astDevices: ARRAY [1..18] OF ST_DeviceInfo
    fb_xxx_axis_pilsdevice_number = parse_line_device_pils_find_motors(lines, debug)

    for old_line in lines:
        eol_string = ""
        this_line_dirty = False
        line_no = line_no + 1
        # determine the line ending
        if old_line.endswith('\r\n'):
            eol_string = "\r\n"
        elif old_line.endswith('\n'):
            eol_string = "\n"
        # strip away the line ending
        old_line = old_line.rstrip("\n")
        old_line = old_line.rstrip("\r")
        # Have the line stripped of EOL: parse it
        # parse "AT MB" lines
        (new_line, found) = parse_line_device_at_mb(old_line, debug)
        if this_line_dirty:
            print("new_line=%s at_at_mbfound=%s device_at_mb_index=%d" % (new_line, found, device_at_mb_index))

        # parse motor devices at memory bytes
        (new_line, found) = parse_line_device_pils_motor_at_mb(new_line, debug)
        # parse the indexer table (entries only)
        (new_line, found) = parse_line_device_pils_offset(new_line, debug)
        if new_line != old_line:
            dirty = True
            this_line_dirty = True
        if this_line_dirty:
            print("old_line(%02d)=%s" % (line_no, old_line))
            print("new_line(%02d)=%s" % (line_no, new_line))
        elif debug >= 3:
            print("____line(%02d)=%s" % (line_no, old_line))
        # re-add LF or CRLF
        new_line = new_line + eol_string
        new_lines_all.append(new_line)
        # end of processing one file
    if device_at_mb_index != pils_device_table_index:
        eol_string = "\r\n"
        err_line = my_script_name + ": error"  + eol_string
        err_line = err_line + my_script_name + ": error" + "Number of devices in the xxx AT %MB table (the first list) = " + str(device_at_mb_index) + eol_string
        err_line = err_line + my_script_name + ": error" + "Number of devices in the indexer table = " + str(pils_device_table_index) + eol_string
        #err_lines.append(err_line)
        dirty = 1
        exit_code = 1

    if debug >= 1:
        print("pathname=%s dirty=%d" % (pathname, dirty))
    if dirty:
        if fix_files:
            file = open(pathname, 'w', newline='', encoding="iso-8859-1")
            file.writelines(new_lines_all)
            if err_line != None:
                file.write(err_line)
            file.close()
    if err_line != None:
        print("%s" % (err_line))

    sys.exit (exit_code)

def main(argv=None):
    global debug
    global my_script_name
    if not argv:
        argv = sys.argv

    argc = len(sys.argv)
    if argc <= 1:
        help_and_exit()
    arg_idx = 0
    my_script_name = sys.argv[arg_idx]
    arg_idx = arg_idx + 1
    debug = 0
    fix_files = False
    while arg_idx < argc:
        if sys.argv[arg_idx] == "--fix":
            fix_files = True
        elif sys.argv[arg_idx] == "--debug":
            debug = debug + 1
        else:
            pathname = sys.argv[arg_idx]
            fix_PILS_GVL_one_file(pathname, debug, fix_files)
        arg_idx = arg_idx + 1


if __name__ == "__main__":
    sys.exit(main(sys.argv))
