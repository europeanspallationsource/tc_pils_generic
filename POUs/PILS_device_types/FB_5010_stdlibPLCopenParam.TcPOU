﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.13">
  <POU Name="FB_5010_stdlibPLCopenParam" Id="{aebf28ef-a5e8-0129-32e2-baaa9746ba9a}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_5010_stdlibPLCopenParam
VAR_INPUT
    stAxisPar    : ST_AxisParameters;
    nPILSDeviceNumber: UINT;
    pstCustomParameterInfo: POINTER TO ST_CustomParameterInfo;
    nStateMachineParamControlIndex : UINT;
    bStdlibReadyForParamIF : BOOL;
    nParamReadCycleCountRead : UINT;
    nParamWriteCycleCountWrite : UINT;
END_VAR

VAR_IN_OUT
    stAxisStruct : ST_AxisStruct;
    nStateMachineParamControlCmd: UINT;
    nNewResetIdleStartBusyStopError : UDINT;
    fValue    : LREAL;
END_VAR

VAR_OUTPUT
    bValidParameter : BOOL;
END_VAR

VAR
    ePermission           : E_Permission;
    eAxisParameter        : E_AxisParameters;
    eMotionFunctionWrite  : E_MotionFunctions;
    bUseCustomParameterInfo : BOOL;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[
eMotionFunctionWrite := E_MotionFunctions.eWriteParameter;
ePermission := E_Permission.eHidden;

bValidParameter := FALSE;
bUseCustomParameterInfo := FALSE;

// Set up things we need later
CASE (nStateMachineParamControlIndex) OF
     32: // UserMin
        bValidParameter := TRUE;
        ePermission := stAxisPar.eUserMinPermission;
        eAxisParameter := E_AxisParameters.SWLimitBackward;
     33: // UserMax
        bValidParameter := TRUE;
        ePermission := stAxisPar.eUserMaxPermission;
        eAxisParameter := E_AxisParameters.SWLimitForward;
     55: // DragError
        bValidParameter := TRUE;
        ePermission := stAxisPar.eLagErrorPermission;
        eAxisParameter := E_AxisParameters.EnablePositionLagMonitoring;
    62: // Idle current. Not supported by all tc_mca_std_lib versions
        bUseCustomParameterInfo := TRUE;
        pstCustomParameterInfo := ADR(GVL_PILS.astDevices[nPILSDeviceNumber].stCustomParameterInfo62);
    64: // Move current. Not supported by all tc_mca_std_lib versions
        bUseCustomParameterInfo := TRUE;
        pstCustomParameterInfo := ADR(GVL_PILS.astDevices[nPILSDeviceNumber].stCustomParameterInfo64);
    192:
        bUseCustomParameterInfo := TRUE;
    193:
        bUseCustomParameterInfo := TRUE;
    194:
        bUseCustomParameterInfo := TRUE;
    195:
        bUseCustomParameterInfo := TRUE;
    196:
        bUseCustomParameterInfo := TRUE;
    197:
        bUseCustomParameterInfo := TRUE;
    198:
        bUseCustomParameterInfo := TRUE;
    199:
        bUseCustomParameterInfo := TRUE;
    200:
        bUseCustomParameterInfo := TRUE;
    201:
        bUseCustomParameterInfo := TRUE;
    202:
        bUseCustomParameterInfo := TRUE;
    203:
        bUseCustomParameterInfo := TRUE;
    204:
        bUseCustomParameterInfo := TRUE;
    205:
        bUseCustomParameterInfo := TRUE;
    206:
        bUseCustomParameterInfo := TRUE;
    207:
        bUseCustomParameterInfo := TRUE;
    208:
        bUseCustomParameterInfo := TRUE;
    209:
        bUseCustomParameterInfo := TRUE;
    210:
        bUseCustomParameterInfo := TRUE;
    211:
        bUseCustomParameterInfo := TRUE;
    212:
        bUseCustomParameterInfo := TRUE;
    213:
        bUseCustomParameterInfo := TRUE;
    214:
        bUseCustomParameterInfo := TRUE;
    215:
        bUseCustomParameterInfo := TRUE;
    216:
        bUseCustomParameterInfo := TRUE;
    217:
        bUseCustomParameterInfo := TRUE;
    218:
        bUseCustomParameterInfo := TRUE;
    219:
        bUseCustomParameterInfo := TRUE;
    //220: // 220 handled in FB_5010_Axis
    //    bUseCustomParameterInfo := TRUE;
    221:
        bUseCustomParameterInfo := TRUE;
    222:
        bUseCustomParameterInfo := TRUE;
    223:
        bUseCustomParameterInfo := TRUE;
    ELSE
        RETURN;
END_CASE


IF bUseCustomParameterInfo THEN
    // Set up the variables used below
    ePermission := pstCustomParameterInfo^.ePermission;
    eAxisParameter := pstCustomParameterInfo^.eAxisParameter;
    // Check permission and the eAxisParameter
    // Note that E_AxisParameters.AxisTargetPosition is a "dummy", never used.
    // In this case, it is not our parameter
    IF ePermission <> E_Permission.eHidden AND eAxisParameter <> E_AxisParameters.AxisTargetPosition THEN
        bValidParameter := TRUE;
    ELSE
        bValidParameter := FALSE;
    END_IF
END_IF

// Not our paramter at all
IF NOT bValidParameter THEN
    RETURN;
END_IF

// Early return when the PLC code has not yet started and initialized
IF nStateMachineParamControlCmd = E_PILS_ParamControlCmd.eParamControlCmdNotInitialized THEN
    RETURN;
END_IF

CASE (nStateMachineParamControlCmd) OF
    E_PILS_ParamControlCmd.eParamControlCmdRead:
        IF ePermission = E_Permission.eHidden THEN
            nStateMachineParamControlCmd := E_PILS_ParamControlCmd.eParamControlCmdErrorReadOnly;
        ELSIF nParamReadCycleCountRead = 1 THEN
            IF bStdlibReadyForParamIF THEN
                // Copy the "command", the "function"
                stAxisStruct.stControl.eCommand := E_MotionFunctions.eReadParameter;
                stAxisStruct.stConfig.eAxisParameters := eAxisParameter;
                stAxisStruct.stControl.bExecute := TRUE;
                nStateMachineParamControlCmd := E_PILS_ParamControlCmd.eParamControlCmdBusy;
            ELSE
                nStateMachineParamControlCmd := E_PILS_ParamControlCmd.eParamControlCmdErrorRetryLater;
            END_IF
        END_IF

    E_PILS_ParamControlCmd.eParamControlCmdWrite:
        IF ePermission <> E_Permission.eWrite THEN
            nStateMachineParamControlCmd := E_PILS_ParamControlCmd.eParamControlCmdErrorReadOnly;
        ELSIF nParamWriteCycleCountWrite = 1 THEN
            // The first time we are in the write cycle
            // Make a decision, if to continue
            IF bStdlibReadyForParamIF THEN
                // Copy the "command", the "function" and the value
                stAxisStruct.stControl.eCommand := eMotionFunctionWrite;
                stAxisStruct.stConfig.eAxisParameters := eAxisParameter;
                stAxisStruct.stConfig.fWriteAxisParameter := fValue;
                stAxisStruct.stControl.bExecute := TRUE;
                nStateMachineParamControlCmd := E_PILS_ParamControlCmd.eParamControlCmdBusy;
            ELSE
                nStateMachineParamControlCmd := E_PILS_ParamControlCmd.eParamControlCmdErrorRetryLater;
            END_IF
        END_IF
    E_PILS_ParamControlCmd.eParamControlCmdBusy:
        // If somebody changed the command or the parameter get out of here
        IF (stAxisStruct.stControl.eCommand <> E_MotionFunctions.eReadParameter AND
            stAxisStruct.stControl.eCommand <> E_MotionFunctions.eWriteParameter) OR
           stAxisStruct.stConfig.eAxisParameters <> eAxisParameter THEN
            nStateMachineParamControlCmd := E_PILS_ParamControlCmd.eParamControlCmdErrorRetryLater;
        ELSIF stAxisStruct.stControl.bExecute THEN
            ; // bExecute still TRUE: std_lib hasn't picked it up (yet), stay here
        ELSIF stAxisStruct.stStatus.bBusy THEN
            nStateMachineParamControlCmd := E_PILS_ParamControlCmd.eParamControlCmdBusy;
        ELSIF stAxisStruct.stStatus.bError OR stAxisStruct.stStatus.bCommandAborted THEN
            nStateMachineParamControlCmd := E_PILS_ParamControlCmd.eParamControlCmdErrorRetryLater;
        ELSIF stAxisStruct.stStatus.bDone THEN
            IF stAxisStruct.stControl.eCommand = E_MotionFunctions.eReadParameter THEN
                fValue := stAxisStruct.stConfig.fReadAxisParameter;
            END_IF
            nStateMachineParamControlCmd := E_PILS_ParamControlCmd.eParamControlCmdDone;
        END_IF
END_CASE
]]></ST>
    </Implementation>
  </POU>
</TcPlcObject>